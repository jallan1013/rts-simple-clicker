﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Woodcutter : MonoBehaviour {

    public Player player;
    public UnitController controller;
    public Button button;
    public TextMeshProUGUI unitText;
    public TextMeshProUGUI costText;
    
    public double uc = 0;
    public double cost_base, cost, cost_total = 0;
    public double ga = 0; // gather ammount/ unit/ interval
    public double income_perc = 0;
    public double gs = 0; // gold/ second

    // Use this for initialization
    void Start()
    {
        cost_base = 10;
        cost = cost_base;
        cost_total = cost;
        ga = Math.Pow(controller.staged_mult, 0);
    }

    // Update is called once per frame
    void Update ()
    {
        gs = ga * uc;
        if (gs != 0)//  && player.gs != 0)
        {
            income_perc = gs / player.gs;
        }
        else
        {
            income_perc = 0;
        }

        TextChange(unitText, costText);

        if (player.gold >= cost * player.buy_count)
        {
            unitText.color = Color.white;
            costText.color = Color.white;
        }
        else
        {
            unitText.color = Color.red;
            costText.color = Color.red;
        }
    }

    public void OnMouseUpAsButton()
    {

        if (player.gold - cost >= 0)
        {
            player.gold -= Math.Round(cost);
            uc += player.buy_count;
            cost *= player.cost_mult;
        }
    }

    void TextChange(TextMeshProUGUI unitText, TextMeshProUGUI costText)
    {
        unitText.SetText("WoodCutter (" + uc + ")");
        costText.SetText(player.buy_count + "x" + Math.Round(cost) + " " + income_perc * 100 + "%");
    }
}


/*
    private void OnMouseEnter ()
    {
        var colors = GetComponent<Button>().colors;
        colors.highlightedColor = Color.white;
    }

    private void OnMouseExit()
    {
        var colors = GetComponent<Button>().colors;
        colors.normalColor = Color.clear;
    }
    */
