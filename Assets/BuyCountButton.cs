﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class BuyCountButton : MonoBehaviour {

    public Player player;
    public TextMeshProUGUI bText;

    // public int bc = 0;
    public string text = "";

	// Use this for initialization
	void Start ()
    {
        // bc = (int)player.buy_count;
        text = "Buy (" + player.buy_count + "x)";
        
    }

    // Update is called once per frame
    void Update ()
    {
        // bc = (int)player.buy_count;
        if (player.buy_count == 0)
        {
            bText.SetText("Buy (MAX)");
        }
        else
        {
            text = "Buy (" + player.buy_count + "x)";
            bText.SetText(text);
        }
    }

    public void OnMouseUpAsButton()
    {
        if (player.buy_count == player.buy_count_def[0])
        {
            player.buy_count = player.buy_count_def[1];
        }
        else if (player.buy_count == player.buy_count_def[1])
        {
            player.buy_count = player.buy_count_def[2];
        }
        else if (player.buy_count == player.buy_count_def[2])
        {
            player.buy_count = player.buy_count_def[3];
        }
        else if (player.buy_count == player.buy_count_def[3])
        {
            player.buy_count = player.buy_count_def[4];
        }
        else if (player.buy_count == player.buy_count_def[4])
        {
            player.buy_count = player.buy_count_def[0];
        }
        else
        {
            player.buy_count = player.buy_count_def[1];
        }
    }
}
