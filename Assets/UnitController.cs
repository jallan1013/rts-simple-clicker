﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitController : MonoBehaviour {

    public Player player;
    public Woodcutter wCutter;
    public Farm farm;
    public Market market;
    public Quarry quarry;
    public Lumbermill lumbermill;
    public Plantation plantation;
    public Factory factory;
    public University university;
    public Highway highway;
    public GameObject wCutterButton, farmButton, marketButton, quarryButton, lmButton, plantButton, factoryButton, universityButton, highwayButton;

    public int frame_count = 0;
    public float fc = 0;
    public double staged_mult = 0;
    double unit_gs = 0;

    // array of script names

    // Use this for initialization
    void Start()
    {
        frame_count = 60;
        fc = Time.deltaTime;
        staged_mult = 10;
    }

    // Update is called once per frame
    void Update()
    {
        fc += Time.deltaTime;

        unit_gs = (wCutter.gs + farm.gs + market.gs + quarry.gs + lumbermill.gs + plantation.gs + factory.gs + university.gs + highway.gs);

        if (fc >= 1f)
        {
            player.gold += unit_gs;
            fc = 0;
        }

        /*
        if (frame_count > 0)
        {
            frame_count -= 1;
        }
        else if (frame_count <= 0)
        {
            player.gs += wCutter.gs;
            frame_count += 60;
        }
        */

        /*
        for (int i = 1; i >= 9; i++) // make a loop here that goes through and passes in scripts to change texts
        {
            TextChange(unit, ec, cc)
        }
        */
    }

    private void OnMouseOver(MonoBehaviour unit)
    {
        
    }
}

    /*void TextChange(UnitController unit, int ec, double cc)
    {
        if (unit.cost >= 1e5)
        {
            while (cc / 10 >= 10) // get down to double digits
            {
                cc /= 10;
                ec += 1;
            }

            if (cc / 10 < 10) // get down to single digits 10 < 0
            {
                if (cc / 10 > 0)
                {
                    cc /= 10;
                    ec += 1;
                }
            }
            else if (cc == 10) // if exactly 10, go again, UNNECESSARY?
            {
                cc /= 10;
                ++ec;
            }

            unit.unitText.SetText("Lumbermill (" + unit.uc + ")");
            unit.costText.SetText(player.buy_count + "x" + cc + "e" + ec + " " + unit.income_perc * 100 + "%");
        }
        else
        {
            unit.unitText.SetText("Lumbermill (" + unit.uc + ")");
            unit.costText.SetText(player.buy_count + "x" + unit.cost + " " + unit.income_perc * 100 + "%");
        }*/

    /*
     * void Gather(MonoBehaviour unit)
    {
        if (frame_count <= 0)
        {
            player.gold += Math.Round(unit.gs);
            // add the gold together here then add it to the player?
        }
    }
    */
