﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour {

    public Woodcutter wCutter;
    public Farm farm;
    public Market market;
    public Quarry quarry;
    public Lumbermill lumbermill;
    public Plantation plantation;
    public Factory factory;
    public University university;
    public Highway highway;

    public TextMeshProUGUI goldText;

    public double gold = 0;
    public double income_total = 0;
    public double gs = 0; // using gs instead of gold_s for brevity, may do a replace all later

    public double buy_count = 0;
    public double[] buy_count_def = new double[5] { 0, 1, 10, 100, 1000 };
    public double cost_mult = 0;
    public double click_mult = 0;

	// Use this for initialization
	void Start ()
    {
        buy_count = 1;
        click_mult = 1;
        cost_mult = 1.05;
	}
	
	// Update is called once per frame
	void Update ()
    {
        income_total = gs;
        gs = wCutter.gs + farm.gs + market.gs + quarry.gs + lumbermill.gs + plantation.gs + factory.gs + university.gs + highway.gs;
        goldText.SetText("Gold: " + gold + "g");
	}

    public void OnMouseUpAsButton()
    {
        gold += 1 * click_mult;
    }

    /*
     player resource : people
     player can buy "managers" (auto miners) with people, can only gen x number of people per second based on housing, larger units cost more managers, cannot buy more than x houses without paid? ala castle crashers builders
     */
}
