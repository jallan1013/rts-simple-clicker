﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Farm : MonoBehaviour
{

    public Player player;
    public UnitController controller;
    public Button button;
    public TextMeshProUGUI unitText;
    public TextMeshProUGUI costText;

    public double uc = 0;
    public double cost_base, cost, cost_total = 0;
    public double ga = 0; // gather ammount/ unit/ interval
    public double income_perc = 0;
    public double gs = 0; // gold/ second

    // Use this for initialization
    void Start()
    {
        cost_base = 1e2;
        cost = cost_base;
        cost_total = cost;
        ga = Math.Pow(controller.staged_mult, 1);
    }

    // Update is called once per frame
    void Update()
    {
        var ec = 1; // exponent calc
        double cc = cost; // cost calc


        gs = ga * uc;
        if (gs != 0)//  && player.gs != 0)
        {
            income_perc = gs / player.gs;
        }
        else
        {
            income_perc = 0;
        }

        TextChange(unitText, costText, ec, cc);

        if (player.gold >= cost * player.buy_count)
        {
            unitText.color = Color.white;
            costText.color = Color.white;
        }
        else
        {
            unitText.color = Color.red;
            costText.color = Color.red;
        }
    }

    public void OnMouseUpAsButton()
    {

        if (player.gold - cost >= 0)
        {
            player.gold -= cost;
            uc += player.buy_count;
            cost = Math.Round(cost * player.cost_mult);
        }
    }

    void TextChange(TextMeshProUGUI unitText, TextMeshProUGUI costText, int ec, double cc)
    {
        if (cost >= 1e5)
        {
            while (cc / 10 >= 10) // get down to double digits
            {
                cc /= 10;
                ec += 1;
            }

            if (cc / 10 < 10) // get down to single digits 10 < 0
            {
                if (cc / 10 > 0)
                {
                    cc /= 10;
                    ec += 1;
                }
            }
            else if (cc == 10) // if exactly 10, go again, UNNECESSARY?
            {
                cc /= 10;
                ++ec;
            }

            unitText.SetText("Farm (" + uc + ")");
            costText.SetText(player.buy_count + "x" + cc + "e" + ec + " " + income_perc * 100 + "%");
        }
        else
        {
            unitText.SetText("Farm (" + uc + ")");
            costText.SetText(player.buy_count + "x" + cost + " " + income_perc * 100 + "%");
        }
    }
}